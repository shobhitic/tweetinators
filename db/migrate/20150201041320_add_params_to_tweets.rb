class AddParamsToTweets < ActiveRecord::Migration
  def change
    add_column :tweets, :params, :text
  end
end
