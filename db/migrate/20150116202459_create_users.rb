class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :provider, limit: 25
      t.string :uid, limit: 50
      t.string :handle
      t.string :token
      t.string :secret
      t.string :email

      t.timestamps null: false
    end
  end
end
