class AddTwitterParamsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :twitter_params, :text
  end
end
