class AddIndexAndLastCalculatedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_calculated_at, :datetime

    add_index :users, [:provider, :uid]
  end
end
