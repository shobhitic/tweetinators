class Tweet < ActiveRecord::Base
  belongs_to :user
  serialize :params

  validates_uniqueness_of :tid
end
