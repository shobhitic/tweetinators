class User < ActiveRecord::Base
  has_many :tweets
  serialize :twitter_params

  def twitter
    @client ||= Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV["TWITTER_KEY"]
      config.consumer_secret     = ENV["TWITTER_SECRET"]
      config.access_token        = token
      config.access_token_secret = secret
    end
  end

  def source_stats
    tweets.group(:source).count
  end

  def self.find_or_create_from_auth_hash(auth)
    user = where(provider: auth.provider, uid: auth.uid).first 
    if user
      user.update twitter_params: auth
    else  
      return create_from_omniauth(auth)
    end
    user
  end

  def self.create_from_omniauth(auth)
    user = create! do |u|
      u.provider = auth["provider"]
      u.uid = auth["uid"]
      u.handle = auth["info"]["nickname"].downcase
      u.token = auth["credentials"]["token"]
      u.secret = auth["credentials"]["secret"]
      u.last_calculated_at = Time.now
      u.twitter_params = auth
    end
    FetchSourceJob.perform_later user.id
    user
  end
end
