class StatsController < ApplicationController
  def username
    @handle = params[:username].downcase
    @user = User.where(handle: @handle).first

    unless @user
      render "stats/not_found"
      return
    end

    @sources = @user.source_stats.select { |k, v| k }.map { |k, v| { source: k, count: v, source_name: /<a(.*)>(.*)<\/a>/.match(k)[2] } }.sort_by { |e| e[:count] }.reverse

    if @user.last_calculated_at < 2.days.ago
      FetchSourceJob.perform_later @user.id
      @user.last_calculated_at = 2.seconds.ago
      @user.save
    end

    respond_to do |format|
      format.html
      format.json { render json: @sources }
    end
  end

  def tweet
    text = params[:tweet]

    SendTweetJob.perform_later current_user.id, text

    respond_to do |format|
      format.js
    end
  end
end
