class PagesController < ApplicationController
  def index
    if current_user
      redirect_to username_path(username: current_user.handle)
    end
  end
end
