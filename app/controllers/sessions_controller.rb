class SessionsController < ApplicationController

  def create
    user = User.find_or_create_from_auth_hash(env["omniauth.auth"])
    session[:user_id] = user.id

    if user.last_calculated_at < 24.hours.ago
      FetchSourceJob.perform_later user.id
    end
    redirect_to follow_us_path, notice: "Signed in!"
  end

  def follow
    @user = current_user
  end

  def follow_me
    FollowMeJob.perform_later current_user.id
    redirect_to username_path(username: current_user.handle)
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Signed out!"
  end

end
