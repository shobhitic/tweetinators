# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.Tweetinators ||= {}

previous_count = 0
current_chart = current_chart = null

get_random_color = () ->
  '#' + (Math.random().toString(16) + '0000000').slice 2, 8

get_sources = ->
  @sources ||= $('#usernames').data('sources').map (e) ->
    value: e.count
    label: e.source_name
    color: get_random_color()
    highlight: get_random_color()
    source: e.source
    count: e.count

convert_sources_to_segments = (sources) ->
  sources.map (e) ->
    value: e.count
    label: e.source_name
    color: get_random_color()
    highlight: get_random_color()
    source: e.source
    count: e.count

set_previous_count = (sources) ->
  count = sources_to_count(sources)
  previous_count = count

sources_to_count = (sources) ->
  sum = 0
  sources.forEach (e) ->
    sum += e.count
  sum

generate_chart = (ctx, sources) ->
  new Chart(ctx).Doughnut(sources, {
    responsive: true,
    animation: false,
    segmentShowStroke: false,
    showTooltips: true
  })

update_chart = (sources) ->
  length = current_chart.segments.length
  for i in [0...length]
    current_chart.removeData()
  for i in convert_sources_to_segments(sources)
    current_chart.addData(i)
  
  

hide_loading = ->
  $('#usernames').hide()

Tweetinators.create_chart = ->
  Chart.defaults.global.responsive = true

  Tweetinators.draw_chart get_sources()
  setTimeout Tweetinators.update_chart, 5000

Tweetinators.draw_chart = (sources) ->

  ctx = $(".chart").get(0).getContext("2d")
  hide_loading()
  set_previous_count sources
  current_chart = generate_chart ctx, sources

Tweetinators.update_chart = ->
  @handle ||= $('#usernames').data('handle')
  request = $.ajax "/#{@handle}.json",
    success: (sources) ->
      console.log "Got the response"
      count = sources_to_count(sources)
      if count > previous_count
        console.log "Now updating!"
        update_chart sources
        setTimeout Tweetinators.update_chart, 5000
  @handle
