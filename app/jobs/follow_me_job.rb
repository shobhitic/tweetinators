class FollowMeJob < ActiveJob::Base
  queue_as :fetch_source

  def perform user_id
    user = User.find user_id
    client = user.twitter

    begin
      client.follow "shobhitic"
    rescue Exception => e
      puts "Found an error"
      puts e
    end
  end
end
