class SendTweetJob < ActiveJob::Base
  queue_as :fetch_source

  def perform(user_id, text)
    user = User.find user_id
    client = user.twitter
    begin
      # client.update text
      puts "========" * 5
      puts "TWEETED"
      puts text
      puts "========" * 5
    rescue Exception => e
      puts "Found an error"
      puts e
    end
  end
end
