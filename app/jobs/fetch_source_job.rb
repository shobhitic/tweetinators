class FetchSourceJob < ActiveJob::Base
  queue_as :fetch_source

  # def perform user_id
  #   user = User.find(user_id)
  #   client = user.twitter

  #   rest_of_tweet_in_db = false
  #   max_id = nil
    
  #   loop do
  #     begin
  #       if max_id
  #         tweets = client.user_timeline count: 200, include_rts: false, trim_user: true, max_id: max_id
  #         break if !tweets.last || max_id == tweets.last.id - 1
  #       else
  #         tweets = client.user_timeline count: 200, include_rts: false, trim_user: true
  #       end
  #     rescue Exception => e
        
  #       break
  #     end

  #     puts tweets.map &:source
  #     tweets.each do |tweet|
  #       t = user.tweets.create source: tweet.source, tid: tweet.id.to_s, params: tweet.to_h
        
  #       if !t.persisted?
  #         rest_of_tweet_in_db = true
  #         break
  #       end
  #     end

  #     break if rest_of_tweet_in_db

  #     max_id = tweets.last.id - 1
  #   end

  #   user.last_calculated_at = DateTime.now
  #   user.save
  # end

  # Testing a new way of putting tweets into db by saving every call as it comes
  # and then creating a job for the next call to twitter. Done to tackle RAM issues in prod.
  def perform user_id, max_id = nil
    user = User.find(user_id)
    client = user.twitter

    rest_of_tweet_in_db = false
    
    begin
      if max_id
        tweets = client.user_timeline count: 200, include_rts: false, trim_user: true, max_id: max_id
        if !tweets.last || max_id == tweets.last.id - 1
          user.last_calculated_at = DateTime.now
          user.save
        end
      else
        tweets = client.user_timeline count: 200, include_rts: false, trim_user: true
      end
    rescue Exception => e
      return
    end

    puts tweets.map &:source
    tweets.each do |tweet|
      t = user.tweets.create source: tweet.source, tid: tweet.id.to_s, params: tweet.to_h
      
      if !t.persisted?
        rest_of_tweet_in_db = true
        break
      end
    end

    if !rest_of_tweet_in_db
      max_id = tweets.last.id - 1
      FetchSourceJob.perform_later user_id, max_id
    else
      user.last_calculated_at = DateTime.now
      user.save
    end
  end
end
