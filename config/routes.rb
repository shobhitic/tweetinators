require 'sidekiq/web'

Rails.application.routes.draw do

  post 'stats/tweet'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')

  get 'i/signout', to: 'sessions#destroy', as: 'signout'
  get 'i/follow', to: 'sessions#follow', as: 'follow_us'
  post 'i/follow', to: 'sessions#follow_me', as: 'follow'

  mount Sidekiq::Web, at: '/notsecure/sidekiq'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  get ':username', to: 'stats#username', as: :username

  # You can have the root of your site routed with "root"
  root 'pages#index'

end
